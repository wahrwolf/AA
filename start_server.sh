#!/usr/bin/env bash

# Make a backup of the database
cp -a ROOT/db.sqlite3 ROOT/db.sqlite3_backup_$(date '+%Y-%m-%d_%H-%M-%S')

# Get newest released version
git checkout master
git pull

# Install exactly all needed dependencies
pipenv sync
pipenv clean

# Make sure all database migrations are applied
pipenv run python ROOT/manage.py migrate

# Start webserver
pipenv run gunicorn ROOT.wsgi --timeout 3000 --chdir ROOT/ --bind :8000
