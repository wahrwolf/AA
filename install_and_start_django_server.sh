#!/bin/bash -x
PORT=8000

pipenv install
pipenv run python ROOT/manage.py makemigrations
pipenv run python ROOT/manage.py migrate
pipenv run python ROOT/manage.py createsuperuser
pipenv run python ROOT/manage.py runserver 0.0.0.0:$PORT
