from django.core.management.base import BaseCommand, CommandError
from ... import scheduler


class Command(BaseCommand):
    help = 'Run scheduled jobs. Should be called every minute by cron.'

    def handle(self, *args, **options):
        scheduler.run_all_tasks()
