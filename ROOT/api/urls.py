from django.conf.urls import url

from . import views

app_name = 'api'

urlpatterns = [
    url(r'^mail_send_next/', views.mail_send_next, name='mail_send_next'),
    url(r'^mail_send_all/', views.mail_send_all, name='mail_send_all'),
    url(r'^msg_stats/', views.msg_stats, name='msgs_still_to_send'),
    url(r'^generate_letters/', views.generate_letters, name='generate_letters'),
    url(r'^billing_cycle/', views.billing_cycle, name='billing_cycle'),
    url(r'^zip_analysis/', views.zip_analysis, name='zip_analysis'),
    url(r'^country_analysis/', views.country_analysis, name='country_analysis'),
    url(r'^search_member_db/', views.search_member_db, name='search_member_db'),
    url(r'^get_erfa_statistics/', views.get_erfa_statistics, name='get_erfa_statistics'),
    url(r'^drop_transactions/', views.drop_transactions, name='drop_transactions'),
    url(r'^address_unknown/', views.bulk_address_unknown, name='api_bulk_address_unknown'),
    url(r'^send_data_record/', views.member_queue_data_record_email, name='send_data_record'),
    url(r'^member_inactivate/', views.member_inactivate, name='member_inactivate'),
    url(r'^member_reactivate/', views.member_reactivate, name='member_reactivate'),
    url(r'^reactivation_reminder/$', views.mass_reactivation_reminder, name='reactivation_reminder'),
    url(r'^no_contact/$', views.no_contact, name='no_contact'),
    url(r'^ga_invitations/$', views.ga_invitations, name='ga_invitations'),
    url(r'^ga_invitations_email/$', views.ga_emails, name='ga_invitations_email'),
    url(r'^datenschleuder_address_stickers/(?P<pa_id>\d+)$', views.datenschleuder_address_stickers,
        name='datenschleuder_address_stickers'),
]
