from django.contrib import admin
from import_app.models import Transaction


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('chaos_number', 'rating', 'status', 'information')


admin.site.register(Transaction, TransactionAdmin)
