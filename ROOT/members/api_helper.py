import datetime
import logging
import csv
from csv import reader, writer

from collections import OrderedDict
from itertools import zip_longest
import io
from dateutil.relativedelta import relativedelta

from .models import EmailAddress, Erfa, Member, EmailToMember, BalanceTransactionLog
from .countryfield import get_country_dict


def uglify_date(date):
    return "{:%d.%m.%y}".format(date)


class MemberCSVHelper:

    fieldnames = ()

    def __init__(self, member_set=None):
        self.member_set = member_set
        self.writer = None
        self.reader = None
        self._fh = None
        self.warnings = []
        self.logs = []

    def do_export(self):
        assert self.member_set is not None, "We need members if we want to export them"
        self.setup_writer()
        for member in self.member_set:
            self.writer.writerow(self.get_row(member))
        result = self._fh.getvalue()
        self._fh.close()
        return result

    def do_import(self, file):
        self._fh = file
        self.setup_reader()
        for row in self.reader:
            self.import_row(row)

    def _get_writer(self,):
        return writer(self._fh)

    def setup_writer(self,):
        if not self._fh:
            self._fh = io.StringIO()
            self.writer = self._get_writer()
            if len(self.fieldnames) > 0:
                self.writer.writerow(self.fieldnames)

    def _get_reader(self,):
        return reader(self._fh)

    def setup_reader(self,):
        if not self._fh:
            assert False, 'No input specified yet. Try using do_import(file_io_object).'
        self.reader = self._get_reader()

    def get_row(self, member):
        raise NotImplementedError

    def import_row(self, row):
        member = self.get_member_from(row)
        if self.update_member(member, row):
            member.save()
            self.logs.append('created/updated: {}'.format(member))

    def get_member_from(self, row):
        raise NotImplementedError

    def update_member(self, member, row):
        raise NotImplementedError


# TODO: Remove. CCC abolished the Erfa Doppelmitgliedschaften as of 12.2017
class ErfaAbgleich(MemberCSVHelper):
    fieldnames = ("Änderung", "Eintrittsdatum CCC", "CNR", "Status", "Vorname",
                  "Nachname", "Anschrift1", "Anschrift2", "Anschrift3", "E-Mail")

    def __init__(self, filename, erfa, member_set=None):
        if member_set is None:
            member_set = erfa.member_set
        self.erfa = erfa
        super().__init__(filename, member_set)

    def get_row(self, member):
        change = ""
        membership_start = member.membership_start
        chaos_number = member.chaos_number
        first_name = member.first_name
        last_name = member.last_name
        address_1 = member.address_1
        address_2 = member.address_2
        address_3 = member.address_3
        country = member.address_country
        email = member.get_primary_mail()
        if not member.is_active:
            status = "Ruhend"
        elif member.membership_type == Member.MEMBERSHIP_TYPE_HONORARY:
            status = "Ehrenmitglied"
        elif member.membership_reduced:
            status = "Ermäßigt"
        else:
            status = "Vollmitglied"
        return (change, membership_start, chaos_number, status, first_name, last_name, address_1, address_2, address_3,
                country, email)

    def get_member_from(self, row):
        if not row[2]:
            return Member()
        return Member.objects.get(pk=int(row[2]))

    def update_member(self, member, row):
        change, membership_start, chaos_number, status, first_name, last_name, address_1, address_2, address_3, country, email = row
        if not change:
            # nothing changed, we do not need to save the model
            return False
        member.first_name = first_name
        member.last_name = last_name
        member.address_1 = address_1
        member.address_2 = address_2
        member.address_3 = address_3
        member.address_country = country
        EmailAddress(member=member, email_address=email, is_primary=True).save()
        if status == "Vollmitglied":
            member.membership_reduced = False
            member.fee_override = None
        elif status == "Ehrenmitglied":
            member.membership_type = Member.MEMBERSHIP_TYPE_HONORARY
            member.membership_reduced = False
            member.fee_override = None
        elif status == "Ermäßigt":
            member.fee_override = None
            member.membership_reduced = True
        elif status == "Ruhend":
            member.is_active = False
            if member.account_balance < 0:
                # member will be saved after this
                member.set_balance_to(0)
                member.fee_paid_until = datetime.date.today()
        elif status == "Austritt Erfa":
            member.alienate(self.erfa)
        elif status == "Austritt CCC":
            # ToDo: set membership_end date to the imported date of the resignation
            pass
        else:
            warn = "Member status unknown, got '{status}': {first} {last} ({pk})".format(
                status=status, first=first_name, last=last_name, pk=chaos_number)
            logging.warning(warn)
            self.warnings.append(warn)

        member.last_update = datetime.date.today()

        return True


class Vereinstisch(MemberCSVHelper):

    # export
    def _get_writer(self,):
        return writer(self._fh, delimiter='\t')

    def _get_reader(self,):
        return reader(self._fh, delimiter='\t')

    def setup_writer(self,):
        if not self._fh:
            self._fh = io.StringIO()
            self.writer = self._get_writer()

    def do_export(self):
        assert self.member_set is not None, "We need members if we want to export them"
        self.setup_writer()
        for member in self.member_set:
            self.writer.writerow(self.get_row(member))

    def get_row(self, member):
        date_format = '%d.%m.%Y'
        emails_queryset = member.emailaddress_set.order_by('-is_primary')
        return (
            member.chaos_number,
            member.first_name,
            member.last_name,
            member.address_1,
            member.address_2,
            member.address_3,
            member.address_country,
            member.erfa.short_name,
            member.address_unknown,
            member.erfa.has_doppelmitgliedschaft,
            member.membership_reduced,
            member.is_active,
            member.membership_type,
            member.membership_start.strftime(date_format),
            member.fee_last_paid.strftime(date_format),
            member.fee_paid_until.strftime(date_format),
            member.last_update.strftime(date_format),
            member.account_balance,
            ','.join(list(emails_queryset.values_list('email_address', flat=True))),
            ','.join(list(emails_queryset.values_list('gpg_key_id', flat=True))),
            member.comment
        )

    def get_member_from(self, row):
        # if there is no chaos number, we create a new Member
        if row[1] == '-1':
            return Member()

        try:
            chaos_number = int(row[1])
        except ValueError:
            warn = 'Ignoring illegal chaos_number: {}. Setting up new member.'.format(row[1])
            logging.warning(warn)
            self.warnings.append(warn)
            return Member()

        try:
            return Member.objects.get(pk=chaos_number)
        except Member.DoesNotExist:
            warn = 'chaos_number not found: {}. Setting up new member.'.format(row[1])
            logging.warning(warn)
            self.warnings.append(warn)
            return Member()

    def _str2date(self, string):
        try:
            return datetime.datetime.strptime(string, '%d.%m.%Y').date()
        except ValueError:
            warn = 'Illegal date: {}.'.format(string)
            logging.warning(warn)
            self.warnings.append(warn)
            return None

    def update_member(self, member, row):
        # (new_member, chaos_number, first_name, last_name, address_1, address_2, address_3, address_country, erfa, email, gpg_key_id, membership_type, membership_reduced, changed, amount_paid, membership_end, Kommentar)

        if row[1] == '-1':
            # new member
            member.membership_type = row[11]
        else:
            # Prepare for re-entering email addresses
            EmailAddress.objects.filter(person=member).delete()

        member.first_name = row[2]
        member.last_name = row[3]
        member.address_1 = row[4]
        member.address_2 = row[5]
        member.address_3 = row[6]
        country = row[7]
        if country in get_country_dict():
            member.address_country = row[7]
        else:
            warn = 'Illegal country code: {}. Defaulted to DE for Germany.'.format(row[7])
            logging.warning(warn)
            self.warnings.append(warn)
        try:
            member.erfa = Erfa.objects.get(short_name=row[8])
        except Erfa.DoesNotExist:
            warn = 'Erfa {} does not exist. Member {} set to Alien'.format(row[8], member.get_name())
            logging.warning(warn)
            self.warnings.append(warn)

        # Membership type cannot be changed and is thus ignored in case of changes to a member

        member.membership_reduced = row[12] == 'true'

        if row[15]:
            membership_end = self._str2date(row[15])
            if membership_end:
                member.membership_end = membership_end
            else:
                return False

        member.comment = row[16].strip()

        member.save()

        try:
            # Save email addresses after the member was saved
            if row[9].strip():
                email_pairs = zip_longest(row[9].split(','), row[10].split(','))
                for email_pair in email_pairs:
                    EmailAddress(person=member, email_address=email_pair[0], gpg_key_id=email_pair[1] or '').save()

            # Process payments after member is fully set-up
            amount_paid = int(row[14])
            if amount_paid > 0:
                member.increase_balance_by(amount_paid, reason=BalanceTransactionLog.MANUAL_BOOKING,
                                           booking_day=member.last_update, comment='Vereinstisch: ' + row[16].strip())
        except:
            member.emailtomember_set.all().delete()
            member.emailaddress_set.all().delete()
            member.delete()
            return False

        return True

# This is not using the MemberCSVHelper base class, because this class seems a bit complicated…
# For the purpose of not accidentally introducing bugs I am not going to refactor this before
# congress, but if I would the implementation would look something like this, so all simple
# modifications could be made through the fieldnames dict (instead of actually having to change any code).
#
# The actual implementation provided here basically implements CashdeskExport(CSVImportExportBase)::export_to_file.
# The main difference here is that we skip importing (we do not require a cashdesk import) and that the implementation
# is not generic (which it does not have to be for now because I am not going to refactor the Vereinstisch export
# to use the same base class this short before 34C3).
#
# ```
# # It should be possible to do
# # with on_warning(my_handler):
# #
# def warn():
#   ...
#
# # Base class used to facilitate customizing various member export & import formats
# class CSVImportExportBase:
#   # List of columns in the exported CSV and their mappings…
#   # This should be some dict like this:
#   #
#   # ```
#   # class MyExport:
#   #   fieldnames = {
#   #     'Eintrittsdatum CCC': 'membership_start',
#   #     'Anschrift1': 'address_1',
#   #     'Status': None
#   #   }
#   # ```
#   #
#   # Basically on the left side is the field in the CSV, on the right
#   # is the field in the actual member model.
#   #
#   # You can overwrite row2member and member2row to define more complex exports & imports
#   # like for Status which has no member field for it's row
#   fieldnames = None
#
#   # Takes a set of members and writes them all
#   # in this CSV format to the given file
#   @classmethod
#   def export_to_file(cls, fh, member_set):
#       writer = csv.DictWriter(fh, fieldnames=cls.fieldnames.keys())
#       writer.writeheader()
#       cls.export_with_writer(writer)
#
#   # Exports the given member set using the given writer.
#   # The writer is some object that supports the `writerow` function,
#   # which takes a dict with all the fields and serializes them *somehow*.
#   #
#   # The writer usually is a csv.DictWriter; be careful to call writeheader()
#   # the writer yourself, because export_with_writer will not call it.
#   #
#   # See export_to_file for an example of how to use this to generate CSV.
#   # You could however also export to json like so:
#   #
#   # ```
#   # class JsonWriter:
#   #   def __init__(fh):
#   #     self._fh = fh
#   #     self._num = 0
#   #
#   #   def writerow(self, data):
#   #     if self._num != 0:
#   #       self._fh.write(',')
#   #     json.dump(data, self._fh)
#   #     self._num += 1
#   #
#   # member_set = ...
#   # fh = ...
#   #
#   # MyCSVExporter.export_with_writer(JsonWriter(fh), member_set)
#   # ```
#   @classmethod
#   def export_with_writer(cls, writer, member_set):
#     for member in member_set:
#       writer.writerow(cls.member2row(member))
#
#   # Takes a member and converts it to a row
#   # for using in export_with_writer.
#   # (the row will be passed to writerow and should be a dict with
#   # all the keys from fieldnames)
#   @classmethod
#   def member2row(cls, member):
#     return {k: getattr(member, v) for k, v in cls.fieldnames.items() if v != None}
#
#   # Takes a file handle that contains CSV data where each row represents
#   # a CCC member.
#   # The CSV file must have a header...
#   #
#   # This does not actually save the imported members, instead an iterator
#   # is returned withe (row, member, save) tuples where the caller should save
#   # each member themselves (save is the reccomendation of the importer whether
#   # the imported document should be saved); this is useful to implement transactional semantics
#   # (store all members in a list; use `with on_warnin(...)`) to throw in case
#   # of any warning; the result would be that no document can be saved if any
#   # document import produced an exception or warning
#   @classmethod
#   def import_from_file(cls, fh):
#     # TODO: Possibly detect whether the input CSV has a header or not
#     return cls.import_with_reader(csv.DictReader(fh))
#
#   # Like import_from_file, but allows the caller to customize the
#   # serialization format.
#   # See import_from_file to see how this is normally used.
#   # This could – again – also be used to implement a JSON based importer
#   # instead of a csv based one; The given reader must be an iterable containing
#   # dict like objects containing the keys from fieldnames...
#   #
#   # ```
#   # fh = ... # Some file handle
#   # data = json.load(fh) # In this case, the JSON must contain an array of objects
#   # members = list(MyCSVExporter.import_with_reader(data))
#   # for (row, member, save) in members:
#   #   if save:
#   #     member.save()
#   # ```
#   @classmethod
#   def import_with_reader(cls, reader):
#     for row in reader:
#       yield cls.row2member(row)
#
#   # This basically takes a row and returns a three-tuple containing
#   # (row, member, save) where save tells the caller whether this particular
#   # member should be saved
#   @classmethod
#   def row2member(cls, row)
#     member = cls.retrieve_member(row)
#     do_save = cls.update_member(row)
#     return (row, member, do_save)
#
#   # Determines the chaos number for a specific row; this is used to
#   # actually retrieve existing members from the database; if -1 is returned,
#   # a new Member is created.
#   # The default implementation looks in cls.fieldnames for the field that
#   # refers to the member with the name 'chaos_number' in the model
#   @classmethod
#   def get_row_chaosno(cls, row):
#     chaosno_field = None
#     for k, v in cls.fieldnames:
#       if v == 'chaos_number':
#         chaosno_field = k
#         break
#
#     chaosno_str = row.get(chaosno_field)
#
#     if chaosno_field == None:
#       throw NotImplementedError("Cannot determine chaosno from fieldnames.")
#
#     return chaosno_str
#
#   @classmethod
#   def retrieve_member(cls, row, _warn):
#     chaosno_str = cls.get_row_chaosno()
#
#     try:
#       chaosno = int(chaosno_str)
#     catch Exception as e:
#       warn('Could not decode chaos number {} because. Setting up new member'.format(chaosno_str, e))
#       return Member()
#
#     if chaosno == -1:
#       return Member()
#
#     try:
#       Member.objects.get(pk=chaosno)
#     catch Exception as e:
#       warn('Could not retrieve member with chaos number {} because {}. Setting up new member'.format(chaosno_str, e))
#       return Member()
#
#   @classmethod
#   def update_member(cls, row, member):
#     for k, v in cls.fieldnames
#       setattr(member, v, row.get(k))
#     return true
# ```
def cashpoint_export_impl(fh, member_set):
    fieldnames = OrderedDict([
        ('chaos_number', 'chaos_number'),
        ('first_name', 'first_name'),
        ('last_name', 'last_name'),
        ('state', None)])

    writer = csv.DictWriter(fh, fieldnames=fieldnames.keys(), dialect='excel-tab')
    writer.writeheader()

    payday = datetime.date.today().replace(month=12, day=1)

    for member in member_set.order_by('chaos_number'):
        row = {k: getattr(member, v) for k, v in fieldnames.items() if v is not None}
        if not member.is_active:
            row['state'] = 'ruhend'
        else:
            row['state'] = 'Verzug' if member.is_payment_late(payday) else 'bezahlt'
        writer.writerow(row)
