from django import forms
from django.forms import inlineformset_factory

from .models import EmailAddress, Member


class BaseAddForm(forms.ModelForm):
    required_css_class = 'required'
    error_css_class = 'alert-danger'


class MemberImportForm(forms.Form):
    docfile = forms.FileField(
        label='Select a file',
        help_text='max. 42 megabytes'
    )


class MemberAddForm(BaseAddForm):
    class Meta:
        model = Member
        fields = (
            'membership_type', 'membership_reduced', 'erfa', 'first_name', 'last_name', 'address_1', 'address_2',
            'address_3', 'address_country', 'comment', 'notification_consent', 'membership_start'
        )
        widgets = {
            'membership_type': forms.Select(attrs={'class': 'form-control'}),
            'erfa': forms.Select(attrs={'class': 'form-control'}),
            'address_country': forms.Select(attrs={'class': 'form-control'}),
            'membership_start': forms.HiddenInput(),
        }


class CashPointImportForm(forms.Form):
    docfile = forms.FileField(
        label='Select a file',
        help_text='max. 42 megabytes'
    )


class EmailAddressFormSet(forms.BaseInlineFormSet):
    """Change each form's behaviour to be required. No empty inline forms allowed."""
    def __init__(self, *args, **kwargs):
        super(EmailAddressFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


class EmailAddressForm(BaseAddForm):
    pass


EmailFormset = inlineformset_factory(
    Member, EmailAddress, formset=EmailAddressFormSet, form=EmailAddressForm,
    fields=('email_address', 'gpg_key_id'), extra=1,
    widgets={'gpg_key_id': forms.TextInput(
        attrs={'oninput': "this.value = this.value.replace(/\s+/g, '');"})
    }
)
