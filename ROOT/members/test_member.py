from django.conf import settings
from django.test import TransactionTestCase, TestCase
from members.models import Member, EmailAddress, Erfa, EmailToMember, BalanceTransactionLog
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from ddt import ddt, data, unpack

from ROOT.settings import FEE


def mock_members(number_of_kinds, chaos_numbers):
    """Creates mock Member database with 4*numberOfKinds+1 members
    Args:
        number_of_kinds:
        chaos_numbers:

    Returns:

    """

    # members with positive account balance
    for i in range(number_of_kinds):
        chaos_number = chaos_numbers.pop(0)
        Member.objects.create(
            chaos_number=chaos_number,
            first_name='Fir' + str(chaos_number),
            last_name='Las' + str(chaos_number),
            account_balance=chaos_number * 1000,
            membership_type=Member.MEMBERSHIP_TYPE_MEMBER
        )

    # members with negative account balance
    for i in range(number_of_kinds):
        chaos_number = chaos_numbers.pop(0)
        Member.objects.create(
            chaos_number=chaos_number,
            first_name='Fir Min' + str(chaos_number),
            last_name='Las Min' + str(chaos_number),
            account_balance=-(chaos_number * 1000),
            membership_type=Member.MEMBERSHIP_TYPE_MEMBER
        )

    for i in range(number_of_kinds):
        # members with fee override and positive account balance
        chaos_number = chaos_numbers.pop(0)
        Member.objects.create(
            chaos_number=chaos_number,
            first_name='Fir Cust' + str(chaos_number),
            last_name='Las Cust' + str(chaos_number),
            account_balance=chaos_number * 1000,
            fee_override=chaos_number * 1000,
            membership_type=Member.MEMBERSHIP_TYPE_MEMBER
        )

        # members with fee override and negative account balance
        chaos_number = chaos_numbers.pop(0)
        Member.objects.create(
            chaos_number=chaos_number,
            first_name='Fir Cust Min' + str(chaos_number),
            last_name='Las Cust Min' + str(chaos_number),
            account_balance=-(chaos_number * 1000),
            fee_override=chaos_number * 1000,
            membership_type=Member.MEMBERSHIP_TYPE_SUPPORTER
        )

    # MEMBERSHIP_TYPE_HONORARY don't bother with money!
    chaos_number = chaos_numbers.pop(0)
    Member.objects.create(
        chaos_number=chaos_number,
        first_name='HONORARY Cust Min' + str(FEE * 5),
        last_name='HONORARY Cust Min' + str(FEE * 5),
        account_balance=-(FEE * 5),
        membership_type=Member.MEMBERSHIP_TYPE_HONORARY
    )


class MockMemberTestCase(TransactionTestCase):

    def setUp(self):
        # how many members of each kind are created? (easier debugging)
        number_of_kinds = 2
        chaos_numbers = list(range(1000))  # pre-generate the chaos_numbers
        mock_members(number_of_kinds, chaos_numbers)
        Member.objects.create(
            chaos_number=133742,
            membership_type=Member.MEMBERSHIP_TYPE_MEMBER,
            erfa=Erfa.objects.create(short_name="abc", long_name="argbleurkccippp"),
            first_name="anton",
            last_name="berger",
            address_1="Under the Bridge 23",
            address_country="DE",
            membership_start=datetime.utcfromtimestamp(1402531200),
            account_balance=4200,
            fee_override=250,
            comment="Dieser Nutzer hat doofe Ohren!"
        )

        for member in Member.objects.all():
            if member.first_name != '':
                email = (member.first_name + member.last_name).replace(' ', '_') + '@' + 'example.com'
                EmailAddress(person=member, email_address=email).save()

    @staticmethod
    def test_isMember():
        member = Member.objects.all().first()
        assert member.is_member()

        member.membership_end = datetime.now().date() - timedelta(days=2)
        assert not member.is_member()

    def test_isMember_equals_member_sonly(self):
        count_members_only = Member.objects.members_only().count()
        count_is_member = 0
        for member in Member.objects.all():
            if member.is_member():
                count_is_member += 1

        self.assertEqual(count_members_only, count_is_member)

    def test_payment(self):
        m = Member.objects.filter(is_active=True).first()
        old_balance = m.account_balance
        m.fee_last_paid -= timedelta(1)  # Making sure it's not set to today
        old_last_paid_date = m.fee_last_paid
        m.increase_balance_by(m.get_annual_fee())
        self.assertEqual(m.account_balance - old_balance, m.get_annual_fee(),
                         'Account balance is being set incorrectly.')
        self.assertEqual(old_last_paid_date, m.fee_last_paid, 'Fee last paid should not have been modified.')

    def test_payment_with_date(self):
        m = Member.objects.filter(is_active=True).first()
        old_balance = m.account_balance
        m.fee_last_paid -= timedelta(1)  # Making sure it's not set to today
        m.increase_balance_by(m.get_annual_fee(), booking_day=date.today())
        self.assertEqual(m.account_balance - old_balance, m.get_annual_fee(),
                         'Account balance is being set incorrectly.')
        self.assertEqual(date.today(), m.fee_last_paid,
                         'Fee last paid should be set to today.')

    def test_person_address_unknown_on_save(self):
        m = Member.objects.first()
        self.assertEqual(m.address_unknown, 0, 'Should be initialized as 0')

        for attr in [('address_1', 'foo'), ('address_2', 'bar'), ('address_3', 'baz'), ('address_country', 'AU')]:
            # Mark as unreachable and save
            m.address_unknown = settings.ADDRESS_RETURNS
            m.save()
            # because only in case the member had been marked as unreachable before the address correction will the
            # counter be reset
            setattr(m, attr[0], attr[1])
            m.save()
            self.assertEqual(m.address_unknown, 0,
                             'Changing {} should reset the address_unknown counter'.format(attr[0]))

    def test_change_balance(self):
        # test modifications to balance
        m = Member.objects.first()
        m.set_balance_to(33)
        self.assertEqual(m.account_balance, 33)
        m = Member.objects.first()  # reload so checking changes vs logging match
        m.increase_balance_by(67)
        self.assertEqual(m.account_balance, 100)
        # test logging
        transaction_logs = BalanceTransactionLog.objects.filter(member=m)
        transaction_log = transaction_logs[len(transaction_logs) - 1]
        self.assertEqual(transaction_log.changed_value, 67)
        self.assertEqual(transaction_log.new_value, 100)

    def test_balance_logging(self):
        # force known balance
        m = Member.objects.first()
        m.set_balance_to(10)
        btl = m.balancetransactionlog_set.all()
        last_btl = list(btl).pop()
        # check that a log has been made
        self.assertGreater(len(btl), 0)
        # check it bares expected value
        self.assertEqual(last_btl.new_value, 10)


@ddt
class MemberTestCase(TestCase):

    def setUp(self):
        self.member = Member(first_name='first', last_name='last', address_1='address', address_country='DE')
        self.member.membership_start = date.today() - relativedelta(years=2)
        self.member.fee_paid_until = self.member.membership_start
        self.member.save()

    @data(
        [date.today() + relativedelta(months=10), -7200, False],
        [date.today() + relativedelta(months=10) - relativedelta(days=1), -7200, True],
        [date.today() + relativedelta(months=10), 0, False],
        [date.today() - relativedelta(months=10), 0, True],
        [date.today() - relativedelta(months=2), 0, False],
        [date.today() + relativedelta(months=10), -7201, True],
    )
    @unpack
    def test_is_payment_late(self, next_fee_due, balance, is_late):
        self.member.fee_paid_until = next_fee_due
        self.member.account_balance = balance
        self.member.save()
        self.assertEqual(self.member.is_payment_late(), is_late)
