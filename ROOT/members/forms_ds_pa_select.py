from django import forms
from members.models import PremiumAddressLabel


class DSAddressLabelForm(forms.Form):
    pa_id = forms.ModelChoiceField(queryset=PremiumAddressLabel.objects.all(), empty_label='None',
                                   label='PremiumAddress-Label', required=False)
