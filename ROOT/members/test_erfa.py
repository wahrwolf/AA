from unittest import TestCase
from members.models import Erfa


class TestErfa(TestCase):

    def test_erfa_short_name(self):
        e = Erfa(short_name='short')
        self.assertIn(e.short_name, str(e))

    def test_erfa_long_name(self):
        e = Erfa(short_name='short', long_name='long')
        self.assertIn(e.short_name, str(e))
        self.assertIn(e.long_name, str(e))

    def test_erfa_short_name_double(self):
        e = Erfa(short_name='short', has_doppelmitgliedschaft=True)
        self.assertIn(e.short_name, str(e))
        self.assertIn('Doppelmitgliedschaft', str(e))

    def test_erfa_long_name_double(self):
        e = Erfa(short_name='short', long_name='long', has_doppelmitgliedschaft=True)
        self.assertIn(e.short_name, str(e))
        self.assertIn(e.long_name, str(e))
        self.assertIn('Doppelmitgliedschaft', str(e))
