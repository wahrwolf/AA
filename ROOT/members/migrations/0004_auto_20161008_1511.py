# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0003_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='balancetransactionlog',
            name='transaction_reason',
            field=models.CharField(choices=[('MAN', 'MANUAL BOOKING'), ('SYN', 'INITIAL IMPORT'), ('BAI', 'BANKING IMPORT'), ('BIC', 'BILLING CYCLE'), ('ACK', 'ACCOUNT CLOSED'), ('NOI', 'NO INFORMATION')], max_length=3),
        ),
        migrations.AlterField(
            model_name='erfa',
            name='short_name',
            field=models.SlugField(unique=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='member',
            name='membership_type',
            field=models.CharField(default='MBR', choices=[('SUP', 'SUPPORTER'), ('MBR', 'MEMBER'), ('HON', 'HONORARY'), ('RMS', 'SUSPENDED')], max_length=3),
        ),
    ]
