from datetime import date

from import_app.models import Transaction
from django.test import TestCase
from members.models import (
    BalanceTransactionLog, EmailToMember,
    Member
)

# Create your tests here.


class BalanceLogTransactionLink(TestCase):

    def setUp(self):
        self.member = Member.objects.create(first_name='first', last_name='last', address_1='address',
                                            membership_type=Member.MEMBERSHIP_TYPE_MEMBER)
        self.transaction = Transaction.objects.create(booking_day=date.today(), available_on=date.today(),
                                                      status=Transaction.STATUS_MATCHED_CHAOS_NR, amount=7200,
                                                      balance=0, chaos_number=self.member.chaos_number, rating=0)

    def test_revert_transaction(self):
        from django.forms import modelformset_factory

        # Book transaction, creating a balance transaction log connected to the imported banking transaction
        TransactionFormSet = modelformset_factory(Transaction, fields=('chaos_number', 'status'), extra=0)
        data = {
            'form-0-id': self.transaction.pk,
            'form-0-chaos_number': self.transaction.chaos_number,
            'form-0-status': self.transaction.status,
            'form-TOTAL_FORMS': 1,
            'form-INITIAL_FORMS': 1
        }
        formset = TransactionFormSet(data=data)
        formset.save()
        self.client.post('/import_app/manage_transactions_book/',
                         {'formset': formset,
                          'status_matching': Transaction.STATUS_MATCHED_CHAOS_NR,
                          'status_erroneous': Transaction.STATUS_UNKNOWN_CHAOS_NR,
                          'form-0-chaos_number': self.transaction.chaos_number,
                          'form-0-status': self.transaction.status, 'form-0-id': self.transaction.pk,
                          'form-TOTAL_FORMS': 1, 'form-INITIAL_FORMS': 1})
        balance_log = BalanceTransactionLog.objects.get(bank_transaction=self.transaction)
        self.assertEqual(balance_log.bank_transaction.status, Transaction.STATUS_COMPLETED)

        # Make an anti transaction. This reverts the balance transaction log and frees the imported banking transaction
        self.client.post('/members/anti_transaction/{}'.format(balance_log.pk),
                         {
                            'member': balance_log.member.chaos_number,
                            'changed_value': balance_log.changed_value,
                            'new_value': balance_log.new_value,
                            'comment': balance_log.comment
                         })

        balance_log = BalanceTransactionLog.objects.get(bank_transaction=self.transaction)
        self.assertNotEqual(balance_log.bank_transaction.status, Transaction.STATUS_COMPLETED)


class EmailToMemberTestCase(TestCase):

    def setUp(self):
        self.member = Member(first_name='first', last_name='last', address_1='address', address_country='DE')
        self.member.save()

    def test_welcome_email(self):
        # creating a member should create a welcome email
        self.assertEqual(self.member.emailtomember_set.count(), 1,
                         'Only one mail should be generated when creating a member.')
        self.assertEqual(self.member.emailtomember_set.first().email_type, EmailToMember.SEND_TYPE_WELCOME,
                         'The first email a member receives should be the welcome mail.')

    def test_welcome_email_after_change(self):
        # a change should not change anything if a welcome mail is in queue
        self.member.address_2 = 'new place'
        self.member.save()
        self.assertEqual(self.member.emailtomember_set.count(), 1,
                         'Even after a change on the member, the welcome mail should be the one and only.')
        self.assertEqual(self.member.emailtomember_set.first().email_type, EmailToMember.SEND_TYPE_WELCOME,
                         'The first email a member receives should be the welcome mail.')

    def test_data_record_after_change(self):
        # multiple changes should only yield one data record mail
        self.member.emailtomember_set.all().delete()

        self.member.address_2 = 'another place'
        self.member.save()
        self.member.address_2 = 'yet another place'
        self.member.save()
        self.assertEqual(self.member.emailtomember_set.count(), 1,
                         'After a data change a data record email should be sent, but only one, even after multiple'
                         'changes.')
        self.assertEqual(self.member.emailtomember_set.first().email_type, EmailToMember.SEND_TYPE_DATA_RECORD,
                         'Mail should be of type data record.')
