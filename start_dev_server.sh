#!/bin/bash -x
PORT=8000

pipenv update
pipenv run python ROOT/manage.py migrate
pipenv run python ROOT/manage.py runserver 0.0.0.0:$PORT
